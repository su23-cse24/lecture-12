#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <iostream>
#include "Board.h"

struct Move {
    int x, y;

    Move(int x, int y) {
        this->x = x;
        this->y = y;
    }
};

struct TicTacToe {
    Board board;
    bool player1;
    bool done;

    Move (*player1Function)(Board, SquareState);
    Move (*player2Function)(Board, SquareState);


    TicTacToe() {
        board = Board();
        player1 = true;
        done = false;
    }

    TicTacToe(int size) {
        board = Board(size);
        player1 = true;
        done = false;
    }

    void setPlayer1(Move (*f)(Board, SquareState)) {
        player1Function = f;
    }

    void setPlayer2(Move (*f)(Board, SquareState)) {
        player2Function = f;
    }

    void start() {
        while(!done) {
            system("clear");
            std::cout << board;

            if (player1) {
                Move move = player1Function(board, PLAYER_1);
                bool success = board.updateGrid(move.x, move.y, PLAYER_1);
                if (!success) {
                    system("clear");
                    std::cout << board;
                    std::cout << "Invalid move" << std::endl;
                    std::cout << "Player 2 wins!" << std::endl;
                    done = true;
                }
            } else {
                Move move = player2Function(board, PLAYER_2);
                bool success = board.updateGrid(move.x, move.y, PLAYER_2);
                if (!success) {
                    system("clear");
                    std::cout << board;
                    std::cout << "Invalid move" << std::endl;
                    std::cout << "Player 1 wins!" << std::endl;
                    done = true;
                }
            }
            player1 = !player1;

            if (board.isWinner(PLAYER_1)) {
                system("clear");
                std::cout << board;
                std::cout << "Player 1 wins!" << std::endl;;
                done = true;
            } else if (board.isWinner(PLAYER_2)) {
                system("clear");
                std::cout << board;
                std::cout << "Player 2 wins!" << std::endl;;
                done = true;
            } else if (board.isFull()) {
                system("clear");
                std::cout << board;
                std::cout << "It's a tie!" << std::endl;;
                done = true; 
            }
        }
    }

};

#endif