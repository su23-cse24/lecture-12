#include<iostream>
#include "TicTacToe.h"
#include "Board.h"
using namespace std;

Move simpleAI(Board board, SquareState player) {
    for (int i = 0; i < board.getSize(); i++) {
        for (int j = 0; j < board.getSize(); j++) {
            if (board.getPos(i, j) == EMPTY) {
                return Move(i, j);
            }
        }
    }
    return Move(0, 0);
}

Move human(Board board, SquareState player) {
    if (player == PLAYER_1) {
        bool success = false;
        int x, y;
        while(!success) {
            std::cout << "Player 1: ";
            std::cin >> x >> y;
            success = board.updateGrid(x, y, PLAYER_1);
        }
        return Move(x, y);
    } else {
        bool success = false;
        int x, y;
        while(!success) {
            std::cout << "Player 2: ";
            std::cin >> x >> y;
            success = board.updateGrid(x, y, PLAYER_2);
        }
        return Move(x, y);
    }
}

int main() {
    system("clear");
    cout << "[h] for Human, [s] for Simple AI" << endl;

    cout << "Select player 1: ";
    char player1;
    cin >> player1;

    cout << "Select player 2: ";
    char player2;
    cin >> player2;

    cout << "Select Board Size: ";
    int size;
    cin >> size;

    
    TicTacToe myGame(size);

    if (player1 == 'h') {
        myGame.setPlayer1(human);
    } else if (player1 == 's') {
        myGame.setPlayer1(simpleAI);
    }

    if (player2 == 'h') {
        myGame.setPlayer2(human);
    } else if (player2 == 's') {
        myGame.setPlayer2(simpleAI);
    }
   
    myGame.start();


    return 0;
}