#ifndef BOARD_H
#define BOARD_H

#include <iostream>

enum SquareState {EMPTY, PLAYER_1, PLAYER_2};

struct Board {
private:
    // member variables
    int size;
    SquareState** grid;

    // allocating memory for grid on the heap
    void initGrid() {
        grid = new SquareState*[size];

        for (int i = 0; i < size; i++) {
            grid[i] = new SquareState[size];
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                grid[i][j] = EMPTY;
            }
        }
    }

public:
    // default constructor
    Board() {
        std::cout << "default constructor" << std::endl;
        size = 3;
        initGrid();
    }

    // overloaded constructor
    Board(int size) {
        std::cout << "overloaded constructor" << std::endl;
        this->size = size;
        initGrid();
    }

    // copy constructor
    // invoked when creating a new instance of a Board,
    // and you set it equal to an existing board
    Board(const Board& other) {
        std::cout << "copy constructor" << std::endl;
        size = other.size;
        initGrid();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                grid[i][j] = other.grid[i][j];
            }
        }
    }

    // overloaded assignment operator
    Board& operator=(const Board& other) {
        std::cout << "overloaded assignmet operator" << std::endl;
        if (size != other.size) {
            for (int i = 0; i < size; i++) {
                delete[] grid[i];
            }
            delete[] grid;

            size = other.size;
            initGrid();
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                grid[i][j] = other.grid[i][j];
            }
        }

        return *this;
    }

    bool updateGrid(int x, int y, SquareState player) {
        if (x < 0 || x >= size || y < 0 || y >= size ) {
            return false;
        }

        if (grid[x][y] != EMPTY) {
            return false;
        }

        grid[x][y] = player;
        return true;
    }

    int getSize() const {
        return size;
    }

    SquareState getPos(int x, int y) const {
        return grid[x][y];
    }

    bool isWinner(SquareState player) {
        // winning rows
        for (int i = 0; i < size; i++) {
            bool fullRow = true;
            for (int j = 0; j < size; j++) {
                if (grid[i][j] != player) {
                    fullRow = false;
                }
            }
            if (fullRow) {
                return true;
            }
        }

        // winning cols
        for (int i = 0; i < size; i++) {
            bool fullCol = true;
            for (int j = 0; j < size; j++) {
                if (grid[j][i] != player) {
                    fullCol = false;
                }
            }
            if (fullCol) {
                return true;
            }
        }

        // checking for top diagonal
        bool topDiagonal = true;
        for (int i = 0; i < size; i++) {
            if (grid[i][i] != player) {
                topDiagonal = false;
            }
        }
        if (topDiagonal) {
            return true;
        }


        // checking for top diagonal
        bool bottomDiagonal = true;
        for (int i = 0; i < size; i++) {
            if (grid[size - 1 - i][i] != player) {
                bottomDiagonal = false;
            }
        }
        if (bottomDiagonal) {
            return true;
        }
        

        return false;
    }

    bool isFull() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (grid[i][j] == EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }


    // destructor
    ~Board() {
        std::cout << "destructor" << std::endl;
        for (int i = 0; i < size; i++) {
            delete[] grid[i];
        }
        delete[] grid;
    }

    // friend std::ostream& operator<<(std::ostream&, const Board&);
};

// overloaded shift left operator (printing board)
std::ostream& operator<<(std::ostream& os, const Board& board){
    os << "   ";
    for (int j = 0; j < board.getSize(); j++){
        os << " " << j << "  ";
    }
    os << std::endl;
    os << "   ";
    for (int j = 0; j < board.getSize(); j++){
        os << "--- ";
    }
    os << std::endl;
    for (int i = 0; i < board.getSize(); i++){
        os << i << " ";
        for (int j = 0; j < board.getSize(); j++){
            char c = ' ';
            if (board.getPos(i, j) == PLAYER_1){
                c = 'X';
            }
            else if (board.getPos(i, j) == PLAYER_2){
                c = 'O';
            }
            os << "| " << c << " ";
            if (j == board.getSize() - 1) os << "|";
        }
        os << std::endl << "   ";
        for (int j = 0; j < board.getSize(); j++){
            os << "--- ";
        }
        os << std::endl;
    }

    return os;
}

#endif